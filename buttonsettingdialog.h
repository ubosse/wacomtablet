#ifndef BUTTONSETTINGDIALOG_H
#define BUTTONSETTINGDIALOG_H

#include <QDialog>

namespace Ui {
class ButtonSettingDialog;
}

class ButtonSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ButtonSettingDialog(QWidget *parent,QString akeyDescription,QString adescription);
    ~ButtonSettingDialog();
    QString keyAction;
    QString keyDescription;
    QString description;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ButtonSettingDialog *ui;
    void keyPressEvent(QKeyEvent*e);
};

#endif // BUTTONSETTINGDIALOG_H

#ifndef TABLETPROFILE_H
#define TABLETPROFILE_H
#include <QtWidgets>
class ButtonProfile
{
public:
  ButtonProfile(){}
  ButtonProfile(short anid, QString akeyAction, QString akeyDescription );
  short id;//das ist die Button-Id, die man mit xev herausfinden kann, die in xsetwacom verwendet wird.
  QString keyAction;//beinhaltet den String, der nach "xsetwacom set $Paddevice Button $id key ..." kommt
  QString keyDescription;//sowas wie "Strg-Z"
  QString description;//Beschreibt die Wirkung
};
class UserProfile{
public:
    QString name="default";
    QRect area;
    ButtonProfile buttons[4];
    bool load(QString adescription="default");//lädt ein Profil aus der Settingsdatei
    void save();//sichert ein Profil in Settings.
    static QStringList getProfileList();

};
class TabletProfile:public QObject
{
    Q_OBJECT
public:
    TabletProfile();
    UserProfile userprofile;
    QString penDevice;
    QString padDevice;//diese Devices sind die die xsetwacom list devices ausgibt
    QStringList areaCommandParameter();//die xsetwacom Parameterliste, die die Area setzt,
    QStringList buttonCommandParameter(ButtonProfile *bp);//die Parameter für den xsetwacom-Befehl für den Button bp
static QString execute(QStringList parameters);//setzt xsetwacom mit den parametern ab.
static QString getProfileName();//öffnet ein Auswahlfeld mit den Profilen, liefert das gewählte zurück.
    void setDefault(QSettings &settings) ;//setzt das default-Profil in den benutzerdefinierten settings.
    bool saved=true;
public slots:
    //void saveDefault();//sichert die Einstellungen als Default-Einstellungen.
    void saveProfile();//sichert das Profil
    void saveProfileAs();//sichert unter neuem Namen
    void loadProfile();//lädt ein neues Profil.
    void executeProfile();//setzt alle xsetwacom-Befehle ab, bzw. schreibt sie in den String shellCommands
    void deleteProfile();//löscht ein Profil aus der Liste.
    void getShellScript();//erzeugt Shell-Skript, das in Datei gespeichert werden kann mit den entsprechenden Profil-Befehlen.
    void changeArea();//ändert die Area.
    void baseSettings();//Grundeinstellungen Pendevice, Paddevice und Area...
};


#endif // TABLETPROFILE_H

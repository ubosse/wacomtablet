#include "tabletdialog.h"
#include "areasettingdialog.h"
#include "tabletprofile.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("WacomTablet");
    a.setOrganizationName("UBoss");
    TabletDialog w;
    TabletDialog::window=&w;
    w.setWindowIcon(QIcon::fromTheme("input-tablet"));
    w.profile.executeProfile();
    w.show();
    return a.exec();
}

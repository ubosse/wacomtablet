#include "basesettingsdialog.h"
#include "ui_basesettingsdialog.h"
#include <QtCore>
#include <QtWidgets>

BaseSettingsDialog::BaseSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BaseSettingsDialog)
{
    ui->setupUi(this);
    QProcess proc;
    proc.start("xsetwacom",QStringList()<<"list"<< "devices");
    proc.waitForFinished();
    QString output=proc.readAllStandardOutput();
    if (output.isEmpty()){
        QMessageBox::warning(0,"Mist",
            QString::fromUtf8("Das Tablet scheint nicht angeschlossen zu sein, "
                              "jedenfalls liefert xsetwacom list devices keine Ausgabe"));
        return;
    }
    ui->konsole->setText(output);
    QStringList devices;
    for(QString& zeile:output.split('\n',Qt::SkipEmptyParts)){
        devices<<zeile.split('\t').at(0).trimmed();
    }
    ui->penDevice->addItems(devices);
    ui->padDevice->addItems(devices);
    //hier nun eine quick-and dirty-Lösung für die Button-Ids
    QSettings settings;
    settings.beginGroup("buttonIds");
    settings.setValue("id1",int(1));
    settings.setValue("id2",int(2));
    settings.setValue("id3",int(3));
    settings.setValue("id4",int(8));
    settings.endGroup();
}

BaseSettingsDialog::~BaseSettingsDialog()
{
    delete ui;
}

QRect BaseSettingsDialog::getActualDeviceArea(QString penDevice)
{
    QSettings settings;
    QProcess proc;
    proc.start("xsetwacom",QStringList()<<"get"<<
               ((penDevice==QString())?settings.value("penDevice").toString():penDevice)<<"Area");
    proc.waitForFinished();
    QString output=proc.readAllStandardOutput();
    QStringList list=output.split(' ',Qt::SkipEmptyParts);
    if(list.count()<4){
        QMessageBox::warning(0,"Mist","da ist was bei der Abfrage der Area schief gegangen.");
        return QRect();
    }
    QRect Area;
    Area.setRect(list.at(0).toInt(),list.at(1).toInt(),list[2].toInt(),list[3].toInt());
    return Area;
}


void BaseSettingsDialog::on_getAreaButton_clicked()
{
    if(ui->penDevice->currentText().isEmpty()){
        QMessageBox::warning(0,"Schlecht","Damit die Area ermittelt werden kann,"
                             "muss ein Pen Device eingetragen sein.");
        return;
    }
    Area=getActualDeviceArea(ui->penDevice->currentText());
    ui->areaLabel->setText(QString::fromUtf8("x-Auflösung: %1\ny-Auflösung: %2").
                           arg(Area.width()).arg(Area.height()));
}

#include "buttonlabel.h"
#include "buttonsettingdialog.h"
#include "tabletdialog.h"

ButtonLabel::ButtonLabel():QLabel(0)
{
   profile=nullptr;
}

ButtonLabel::ButtonLabel(QWidget *parent, ButtonProfile *bP):QLabel(parent)
{
    profile=bP;
    QString text=QString("%1\n (%2)").arg(bP->keyDescription).arg(bP->description);
    setText(text);
    setGeometry(0,0,115,35);
    setAlignment(Qt::AlignHCenter);
    setFocusPolicy(Qt::ClickFocus);
    //setStyleSheet("background-color:rgb(255,255,255,180); font-size:8pt");
    setStyleSheet("color:rgb(255,255,255); font-size:8pt");
    setWindowOpacity(0.3);
}
void ButtonLabel::mousePressEvent(QMouseEvent*ev)
{
  ButtonSettingDialog * bs = new ButtonSettingDialog((QWidget*)parent(),
                                      profile->keyDescription,profile->description);
  if(bs->exec()==QDialog::Accepted){
      profile->description=bs->description;
      profile->keyAction=bs->keyAction;
      profile->keyDescription=bs->keyDescription;
      setText(QString("%1\n (%2)").arg(profile->keyDescription).
              arg(profile->description));
      TabletDialog*td=qobject_cast<TabletDialog*>(parentWidget());
      TabletProfile::execute(
        td->profile.buttonCommandParameter( profile));
      td->profile.saved=false;
  }
  ev->accept();
}


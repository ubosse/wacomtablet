#include "tabletdialog.h"
#include "buttonlabel.h"
TabletDialog*TabletDialog::window=0;
QTextEdit* TabletDialog::log=nullptr;//zum öffentlichen Zugriff auf das Logfenster.
TabletDialog::TabletDialog(QWidget *parent) :
    QDialog(parent)
{
    setObjectName("TabletDialog");
    setStyleSheet("QDialog#TabletDialog {background-image:url(:/tabletFoto.png);"
            "background-repeat:no-repeat;"
            "background-position:top left;}");
    setWindowTitle(QString::fromUtf8("Tablet-Profil: ")+profile.userprofile.name);
    setFixedSize(581,453);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    for(short i=0;i<4;i++){
        ButtonLabel * bL=new ButtonLabel(this,&profile.userprofile.buttons[i]);
        bL->move(i*120+54,47);
        bL->show();
        buttonLabel[i]=bL;
    }
    //Jetzt ein Menu bauen
    QMenuBar * menuBar=new QMenuBar(this);

    menuBar->setStyleSheet("QMenuBar::item{color:white}");
    menuBar->move(195,0);
    QMenu * profilMenu=menuBar->addMenu("&Profile");
    //profilMenu->setStyleSheet("QMenu{title-color:white} ");
    QMenu * settingMenu=menuBar->addMenu("&Einstellungen");
    profilMenu->addAction("Profil sichern",&profile,SLOT(saveProfile()),QKeySequence::Save);
    profilMenu->addAction("Profil sichern als...",&profile,SLOT(saveProfileAs()),QKeySequence::SaveAs);
    profilMenu->addAction("Profil laden",&profile,SLOT(loadProfile()),QKeySequence::Open);
    profilMenu->addAction(QString::fromUtf8("Profil löschen"),&profile,SLOT(deleteProfile()));
    profilMenu->addSeparator();
    profilMenu->addAction("Shell Skript erzeugen",&profile,SLOT(getShellScript()));
    settingMenu->addAction("Stiftempfindlichkeit einstellen...",&profile,SLOT(changeArea()));
    settingMenu->addAction("Grundeinstellungen zur Ersteinrichtung",&profile,SLOT(baseSettings()));
//jetzt noch das Logfeld
    log=new QTextEdit(this);
    log->setReadOnly(true);
    log->setText("Abgesetzte Befehle:\n");
    log->resize(500,200);
    log->move(30,110);
/*----Button zum reload der Einstellungen------------*/
    QPushButton * reloadButton=new QPushButton(QIcon::fromTheme("edit-redo"),"reload",this);
    reloadButton->setToolTip(QString::fromUtf8("führt die xsetwacom-Befehle noch einmal aus. Das kann "
                             " nötig sein, wenn das Tablet neu erkannt wurde."));
    reloadButton->move(width()/2-reloadButton->width()/2,height()-80);
    connect(reloadButton,SIGNAL(clicked()),&profile,SLOT(executeProfile()));
}

TabletDialog::~TabletDialog()
{

}

void TabletDialog::adjustButtonLabels()
{
    QSettings settings;
    settings.beginGroup( profile.userprofile.name);
    for(short i=0;i<4;i++){
        settings.beginGroup(QString("button%1").arg(i+1));
        buttonLabel[i]->setText(settings.value("keyDescription").toString()+"\n"+settings.value("description").toString());
        settings.endGroup();
    }
    settings.endGroup();
}

void TabletDialog::closeEvent(QCloseEvent *ev)
{
    QSettings settings;
    settings.setValue("lastProfile",profile.userprofile.name);
    if(profile.saved){
        ev->accept();
        return;
    }
   if( QMessageBox::question(0,"Frage",QString::fromUtf8("Das Tabletprofil wurde noch nicht gesichert\n"
                          "Anwendung wirklich schließen?"),
                          QMessageBox::No | QMessageBox::Default,
                          QMessageBox::Yes)
           ==QMessageBox::Yes){
       ev->accept();
   }else{
       ev->ignore();
   }
}



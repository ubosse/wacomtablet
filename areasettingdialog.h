#ifndef AREASETTINGDIALOG_H
#define AREASETTINGDIALOG_H
#include <QtWidgets>


class TabletProfile;
class MessWidget;

namespace Ui {
class AreaSettingDialog;
}

class AreaSettingDialog : public QDialog
{
    Q_OBJECT

public:
    AreaSettingDialog(QWidget*parent);
    explicit AreaSettingDialog(TabletProfile * aprofile);
    ~AreaSettingDialog();
    TabletProfile*profile;
    QSize wacomArea,screenArea;



private slots:
    void on_startCalibration_clicked();
    void messWidgetClosed();

private:
    Ui::AreaSettingDialog *ui;
    MessWidget* messWidget;
};

class MessWidget : public QDialog
{
    Q_OBJECT
public:
    MessWidget();
    QPoint topleft=QPoint(0,0);
    QPoint bottomright=QPoint(0,0);
    QSize screensize;
public:
    QSize getSize(){return QSize(bottomright.x()-topleft.x(),bottomright.y()-topleft.y());}
private:
    bool signalEmitted=false;
    void tabletEvent(QTabletEvent *event);
signals:
    void penReleased();
};

class QuadratDialog : public QDialog
{
    Q_OBJECT
public:
    explicit QuadratDialog(QWidget *parent = nullptr);
private:
    QPushButton * okButton;
    QLabel*sizeLabel;
    void resizeEvent(QResizeEvent *ev);
private slots:
    void quadriere();
};

#endif // AREASETTINGDIALOG_H

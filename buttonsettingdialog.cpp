#include "buttonsettingdialog.h"
#include "ui_buttonsettingdialog.h"
#include <QtWidgets>

ButtonSettingDialog::ButtonSettingDialog(QWidget *parent, QString akeyDescription, QString adescription) :
    QDialog(parent),
    ui(new Ui::ButtonSettingDialog)
{
    ui->setupUi(this);
    keyDescription=akeyDescription;
    ui->shortcut->setText(keyDescription);
    description=adescription;
    ui->description->setText(description);
    ui->shortcut->setFocusPolicy(Qt::StrongFocus);
    ui->shortcut->setFocus();
    for(QAbstractButton*but:ui->buttonBox->buttons())
        but->setShortcut(QKeySequence());

}

ButtonSettingDialog::~ButtonSettingDialog()
{
    delete ui;
}

void ButtonSettingDialog::keyPressEvent(QKeyEvent *e)
{
    if(((e->key()>=0x100)&&(e->key()<Qt::Key_F1)) || (e->key()>Qt::Key_F35) ){
        e->ignore();
        return;
    }
    QString desc;
    QString preaction="key ";QString postaction="";
    QString modifier="";
    if(e->modifiers()&Qt::ShiftModifier){
        modifier+="Shift-";
        preaction+="+shift ";
        postaction=" -shift"+postaction;
    }
    if(e->modifiers()&Qt::ControlModifier){
        modifier+="Strg-";
        preaction+="+ctrl ";
        postaction=" -ctrl"+postaction;
    }
    if(e->modifiers()&Qt::KeypadModifier){
        modifier+="keypad-";
        preaction+="+keypad ";
        postaction=" -keypad"+postaction;
    }
    if(e->modifiers()&Qt::AltModifier){
        modifier+="Alt-";
        preaction+="+alt ";
        postaction=" -alt"+postaction;
    }
    if(e->modifiers()&Qt::GroupSwitchModifier){
        modifier+="AltGr-";
        preaction+="+altgr ";
        postaction=" -altgr"+postaction;
    }
    if (e->key()>=Qt::Key_F1 && (e->key()<=Qt::Key_F35)){
        desc=QString("F")+QString::number(e->key()-Qt::Key_F1+1);
        keyAction=QString("key f")+QString::number(e->key()-Qt::Key_F1+1);
    }else{
        desc=modifier+QChar::fromLatin1(e->key());
        keyAction=preaction+QChar::fromLatin1(e->key())+postaction;
    }
    keyDescription=desc;
    ui->shortcut->setText(desc);
    ui->keyAction->setText(keyAction);
    e->accept();
}

void ButtonSettingDialog::on_buttonBox_accepted()
{
    description=ui->description->text();
}

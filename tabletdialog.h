#ifndef TABLETDIALOG_H
#define TABLETDIALOG_H
#include "tabletprofile.h"
#include "buttonlabel.h"
#include <QDialog>

namespace Ui {
class TabletDialog;
}

class TabletDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TabletDialog(QWidget *parent = 0);   
    ~TabletDialog();
    TabletProfile profile;
    ButtonLabel*buttonLabel[4];
    static TabletDialog*window;
    void adjustButtonLabels();//passt nach Profiländerung die ButtonLabels an.
static  QTextEdit*log;

private:
    Ui::TabletDialog *ui;
    void closeEvent(QCloseEvent *ev);
};

#endif // TABLETDIALOG_H

#include "areasettingdialog.h"
#include "ui_areasettingdialog.h"
#include "tabletprofile.h"
#include "basesettingsdialog.h"
AreaSettingDialog::AreaSettingDialog(QWidget*w):QDialog(w){
    qDebug()<<"das dürfte eigentlich nie aufgerufen werden.";
}
AreaSettingDialog::AreaSettingDialog(TabletProfile *aprofile) :
    QDialog(0),
    ui(new Ui::AreaSettingDialog)
{
    ui->setupUi(this);
    profile=aprofile;
    QSettings settings("UBoss","WacomTablet");
    QRect deviceArea=settings.value("deviceArea",QRect(0,0,0,0)).toRect();
    if (deviceArea.width()==0){
        QMessageBox::warning(0,"Achtung","Bitte erst die Grundeinstellungen zur"
                             "Ersteinrichtung vornehmen!");
        return;
    }

    resize(sizeHint());
}

AreaSettingDialog::~AreaSettingDialog()
{
    delete ui;
}

MessWidget::MessWidget():QDialog(0)
{
    //QDesktopWidget* desktop=QApplication::desktop();
    screensize=//desktop->screenGeometry().size();
            QGuiApplication::screens().at(0)->geometry().size();
    setGeometry(screensize.width()/6,screensize.height()/6,
                2*screensize.width()/3,2*screensize.height()/3);
    QLabel *info=new QLabel("Platziere den Cursor in dieses Fenster\n"
                "Zeichne dann auf dem Wacom-Grafiktablett\n"
                "die Diagonale eines Quadrates\n"
                "von oben links nach unten rechts.",this);
    info->resize(info->sizeHint());
    info->move(0,0);
    info->show();
}

void MessWidget::tabletEvent(QTabletEvent *event)
{
    //qDebug()<<"Tablet-Event-Type: "<<event->type();
    event->accept();
    if(event->type()==QEvent::TabletPress){
        setStyleSheet("background-color:green");
        topleft=event->pos();
        qDebug()<<"topleft "<<topleft;
    }
    if(event->type()==QEvent::TabletRelease){
        if(signalEmitted) return;
        setStyleSheet("");
        bottomright=event->pos();
        qDebug()<<"bottomRight"<<bottomright;
        signalEmitted=true;
        emit penReleased();
    }
}

QuadratDialog::QuadratDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *lay=new QVBoxLayout(this);
    QLabel*textLabel=new QLabel(QString::fromUtf8("Bringe diese grüne Fläche auf die Größe\n"
                                                  "des Quadrates, dessen Diagonale du\n"
                                                  "auf das Tablet gezeichnet hast."),this);
    textLabel->setMinimumSize(QSize(10,10));
    lay->addWidget(textLabel);
    sizeLabel=new QLabel(QString::fromUtf8("Größe: "),this);
    sizeLabel->setStyleSheet("border:1px;padding:4px");
    sizeLabel->setMinimumSize(QSize(10,10));
    lay->addWidget(sizeLabel,0,Qt::AlignCenter);
    QPushButton * but=new QPushButton(QString::fromUtf8("Höhe:=Breite!"),this);
    connect(but,SIGNAL(clicked(bool)),this,SLOT(quadriere()));
    but->setMinimumSize(QSize(20,20));
    lay->addWidget(but,0,Qt::AlignCenter);
    okButton=new QPushButton("Ok!",this);
    okButton->setMinimumSize(20,20);
    //okButton->setEnabled(false);
    connect(okButton,SIGNAL(clicked(bool)),this,SLOT(accept()));
    lay->addWidget(okButton,0,Qt::AlignCenter);
    lay->addStretch(1);
    quadriere();
    setStyleSheet("background-color:rgb(128,255,128)");
    //connect(this,SIGNAL(resizing(int)),this,SLOT(quadriere(int)),Qt::DirectConnection);
}

void QuadratDialog::resizeEvent(QResizeEvent *ev){
    //qDebug()<<"resizeEvent: ev.size: "<<ev->size();
    sizeLabel->setText(QString::fromUtf8("Grösse: %1 x %2").arg(ev->size().width()).arg(ev->size().height()));
}
void QuadratDialog::quadriere()
{
    resize(size().width(),size().width());
}


void AreaSettingDialog::on_startCalibration_clicked()
{
    messWidget=new MessWidget;
    connect(messWidget,SIGNAL(penReleased()),this,SLOT(messWidgetClosed()));
    messWidget->setAttribute(Qt::WA_DeleteOnClose);
    messWidget->exec();

}

void AreaSettingDialog::messWidgetClosed()
{
    wacomArea=messWidget->getSize();
    messWidget->close();
    profile->userprofile.area=BaseSettingsDialog::getActualDeviceArea();
    //qDebug()<<"messWidgetClosed: wacomArea ist "<<wacomArea;
    QuadratDialog qd;
    qd.resize(wacomArea);
    if(qd.exec()==QDialog::Accepted){
        screenArea=qd.size();
        QSize newSize=QSize(profile->userprofile.area.width()*wacomArea.width()/screenArea.width(),
                   profile->userprofile.area.height()*wacomArea.height()/screenArea.height());
        profile->userprofile.area.setSize(newSize);
        profile->execute(profile->areaCommandParameter());
        QMessageBox::information(0,"Erledigt","Habe die Bewegungsempfindlichkeit des Stiftes angepasst.\n"
                                 "Wenn dies dauerhaft so bleiben soll: nicht vergessen, das Profil zu sichern.");
        profile->saved=false;
        //qDebug()<<"wacomArea: "<<wacomArea<<"screenArea: "<<screenArea;
    }
}

#ifndef BUTTONLABEL_H
#define BUTTONLABEL_H
#include "tabletprofile.h"

class ButtonLabel : public QLabel
{
public:
    ButtonLabel();
    ButtonLabel(QWidget * parent,ButtonProfile*bP);
private:
    ButtonProfile * profile;
    void mousePressEvent(QMouseEvent *ev);

};

#endif // BUTTONLABEL_H

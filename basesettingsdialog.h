#ifndef BASESETTINGSDIALOG_H
#define BASESETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class BaseSettingsDialog;
}

class BaseSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BaseSettingsDialog(QWidget *parent = 0);
    ~BaseSettingsDialog();
    QRect Area;
public:
    Ui::BaseSettingsDialog *ui;
    static QRect getActualDeviceArea(QString penDevice=QString());
private slots:
    void on_getAreaButton_clicked();
};

#endif // BASESETTINGSDIALOG_H

#include "tabletprofile.h"
#include "tabletdialog.h"
#include "basesettingsdialog.h"
#include "ui_basesettingsdialog.h"
#include "areasettingdialog.h"
#include "ui_areasettingdialog.h"

TabletProfile::TabletProfile()
{
   QSettings settings;
   while(!settings.contains("penDevice")){
       QMessageBox::warning(0,"Achtung","Es wurde noch kein Tablet eingerichtet. Rufe den Einrichtungsdialog jetzt auf");
       setDefault(settings);
   }
   QString prof=settings.value("lastProfile").toString();
   userprofile.load(prof );
   penDevice=settings.value("penDevice").toString();
   padDevice=settings.value("padDevice").toString();
   /*area=settings.value("area").toRect();
   for(short i=0;i<4;i++){
       QString button=QString("button%1").arg(i+1);
       buttons[i].id=settings.value(button+"/id").toInt();
       buttons[i].keyAction=settings.value(button+"/keyAction").toString();
       buttons[i].keyDescription=settings.value(button+"/keyDescription").toString();
       buttons[i].description=settings.value(button+"/description").toString();
   }*/
}

QStringList TabletProfile::areaCommandParameter()
{
    QStringList pars;
    pars<<"set"<<penDevice<<"Area"<<QString::number(userprofile.area.x())<<QString::number(userprofile.area.y())
       <<QString::number(userprofile.area.width())<<QString::number(userprofile.area.height());
    return pars;
}

QStringList TabletProfile::buttonCommandParameter(ButtonProfile*bp)
{
    QStringList par;
    par<<"set"<<padDevice<<"Button"<<QString::number(bp->id)<<bp->keyAction;
    return par;
}

QString TabletProfile::execute(QStringList parameters)
{
    QProcess proc;
    proc.start("xsetwacom",parameters);
    proc.waitForFinished();
    QString ret=proc.readAll();
    TabletDialog::log->append("setwacom"+parameters.join(' ')+"\n");
    return ret;
}
void TabletProfile::executeProfile(){
    QString commands=QString("xsetwacom set \"%1\" Mode Relative").arg(penDevice);
    QProcess proc;
    proc.start("xsetwacom",QStringList()<<"set"<<penDevice<<"Mode"<<"Relative");
    proc.waitForFinished();
    QByteArray errorMsg=proc.readAllStandardError();
    if(errorMsg.size()>2){
        QTextEdit & logger= *TabletDialog::log;
        logger.moveCursor(QTextCursor::End);
        logger.setTextColor(QColor("red"));
        logger.append(QString::fromUtf8("Fehler bei Ausführung von \n%1 : \n").
                                  arg(commands));
        logger.append(errorMsg);
        logger.setTextColor("black");
    }
    proc.start("xsetwacom",areaCommandParameter());
    commands+="\n"+areaCommandParameter().join(' ');
    proc.waitForFinished();
    for(short i=0;i<4;i++){
        proc.start("xsetwacom",buttonCommandParameter(&userprofile.buttons[i]));
        commands+="\n"+buttonCommandParameter(&userprofile.buttons[i]).join(' ');
        proc.waitForFinished();
    }
    //jetzt noch die Beschleunigung deaktivieren:
    proc.start("xset",QStringList()<< "m"<< "1");
    commands+="\nxset m 1";
    proc.waitForFinished();
    TabletDialog::log->append(commands);
}

void TabletProfile::getShellScript()
{
    QString skript="#!/bin/bash\n";
    skript+=QString("xsetwacom set \"%1\" Mode Relative\n").arg(penDevice);
    skript+=areaCommandParameter().join(' ')+"\n";
    for(short i=0;i<4;i++){
        skript+=buttonCommandParameter(&userprofile.buttons[i]).join(' ')+"\n";
    }
    QString dir=QDir::homePath();
    if(QDir(dir+"/bin").exists())
        dir+="/bin";
    QString filename=QFileDialog::getSaveFileName(0,"Dateiname für Skript",dir);
    if(filename.isEmpty()) return;
    QFile f(filename);
    f.open(QIODevice::ReadWrite);
    QTextStream str(&f);
    str<<skript;
    f.close();
    QMessageBox::information(0,"Info",QString::fromUtf8("Folgendes Skript wurde gesichert:\n%1").arg(skript));
}
void TabletProfile::setDefault(QSettings&settings)
{
    QMessageBox::warning(0,"Mist","Hier ist noch nicht alles perfekt.");
   // settings.setValue("penDevice","Wacom Intuos S Pen stylus");
   // settings.setValue("padDevice","Wacom Intuos S Pad pad");
   // settings.setValue("area",QRect(0,0,56700,32400));
    settings.setValue("lastProfile","Xournal++");
    settings.setValue("profiles",QStringList()<<"Xournal++");
    settings.beginGroup("Xournal++");
    settings.setValue("button1/keyAction","key +ctrl 6 -ctrl");
    settings.setValue("button1/keyDescription","Strg-6");
    settings.setValue("button1/description","Linie");
    settings.setValue("button2/keyAction","key +ctrl 1 -ctrl");
    settings.setValue("button2/keyDescription","Strg-1");
    settings.setValue("button2/description","Formen erkennen");
    settings.setValue("button3/keyAction","key +ctrl 5 -ctrl");
    settings.setValue("button3/keyDescription","Strg-5");
    settings.setValue("button3/description","Koordinatensystem");
    settings.setValue("button4/keyAction","key +ctrl z -ctrl");
    settings.setValue("button4/keyDescription","Strg-Z");
    settings.setValue("button4/description","Rückgängig");
    settings.endGroup();
    settings.sync();
    baseSettings();
    QMessageBox::information(0,"Hinweis","jetzt unter Einstellungen gleich die Stiftempfindlichkeit einstellen");
}


void TabletProfile::saveProfile()
{
    userprofile.save();
    saved=true;
}

void TabletProfile::saveProfileAs()
{
    QDialog*dial=new QDialog;
    QFormLayout*lay=new QFormLayout(dial);
    QLineEdit*lE=new QLineEdit(dial);
    lay->addRow("Layoutbezeichnung",lE);
    QPushButton * but=new QPushButton("Ok",dial);
    connect(but,SIGNAL(clicked(bool)),dial,SLOT(accept()));
    lay->addWidget(but);
    dial->setLayout(lay);
    if(dial->exec()==QDialog::Accepted){
        userprofile.name=lE->text();
    }
    delete dial;
    userprofile.save();
    saved=true;
    TabletDialog::window->setWindowTitle(userprofile.name);
}
QString TabletProfile::getProfileName(){
    QSettings settings;
    QStringList profile=settings.value("profiles").toStringList();
    QDialog dial;
    QVBoxLayout* lay=new QVBoxLayout(&dial);
    lay->addWidget(new QLabel(QString::fromUtf8("zu ladendes Profil wählen")));
    QComboBox*combo=new QComboBox(&dial);
    combo->addItems(profile);
    lay->addWidget(combo);
    QPushButton*but=new QPushButton("Ok",&dial);
    lay->addWidget(but,Qt::AlignHCenter);
    connect(but,SIGNAL(clicked(bool)),&dial,SLOT(accept()));
    return (dial.exec()==QDialog::Accepted)?combo->currentText():QString();
}
void TabletProfile::loadProfile()
{
    QString name=getProfileName();
    if(name==QString())
        QMessageBox::warning(0,"Achtung","Kein Profil geladen.");
    else{
        userprofile.load(name);
        executeProfile();
        TabletDialog::window->adjustButtonLabels();
    }
}

void TabletProfile::deleteProfile()
{
   QString name=getProfileName();
   QSettings settings;
   QStringList profiles=settings.value("profiles").toStringList();
   profiles.removeOne(name);
   settings.setValue("profiles",profiles);
   settings.remove(name);
   settings.sync();
}

void TabletProfile::changeArea()
{
    AreaSettingDialog * dial=new AreaSettingDialog(this);
    dial->exec();
    saved=false;
}

void TabletProfile::baseSettings()
{
    QSettings settings;
    BaseSettingsDialog * dial=new BaseSettingsDialog(0);
    //dial->ui->penDevice->setText(settings.value("penDevice","noch nicht ermittelt").toString());
    //dial->ui->padDevice->setText(settings.value("padDevice","noch nicht ermittelt").toString());
    QRect area=settings.value("deviceArea",QRect(0,0,1000,1000)).toRect();
    //dial->Area=area;
    dial->ui->areaLabel->setText(QString::fromUtf8("x-Auflösung: %1\ny-Auflösung: %2").
                                 arg(area.width()).arg(area.height()));
    if(dial->ui->konsole->toPlainText().isEmpty()){
        delete dial;
        return;
    }

    if(dial->exec()==QDialog::Accepted){
        settings.setValue("padDevice",dial->ui->padDevice->currentText());
        settings.setValue("penDevice",dial->ui->penDevice->currentText());
        settings.setValue("deviceArea",dial->Area);
        QString lastProfile=settings.value("lastProfile","Xournal++").toString();
        settings.setValue(lastProfile+"/area",dial->Area);
    }
    saved=true;
    delete dial;
}

QStringList UserProfile::getProfileList()
{
    QSettings settings;
    return settings.value("profiles").toStringList();
}


ButtonProfile::ButtonProfile(short anid, QString akeyAction, QString akeyDescription)
{
    id=anid;
    keyAction=akeyAction;
    keyDescription=akeyDescription;
}

bool UserProfile::load(QString adescription)
{
    QSettings settings;
    name=adescription;
    bool retvalue=true;
    if(adescription=="default" || !settings.value("profiles").toStringList().contains(name)){
        if(adescription!="default") retvalue=false;
        name=settings.value("defaultProfile",QString("default")).toString();
    }
    settings.beginGroup("buttonIds");
    for(short i=0;i<4;i++){
        buttons[i].id=settings.value(QString("id%1").arg(i+1)).toInt();
    }
    settings.endGroup();
    settings.beginGroup(name);
    area=settings.value("area").toRect();
    for(short i=0;i<4;i++){
        QString but=QString("button%1/").arg(i+1);
        buttons[i].description=settings.value(but+"description").toString();
        buttons[i].keyAction=settings.value(but+"keyAction").toString();
        buttons[i].keyDescription=settings.value(but+"keyDescription").toString();
    }
    settings.endGroup();
    if(retvalue && TabletDialog::window !=0) TabletDialog::window->setWindowTitle(name);
    return retvalue;
}

void UserProfile::save()
{
    QSettings settings;
    QStringList profL=getProfileList();
    if (!profL.contains(name)){
        profL.append(name);
        settings.setValue("profiles",profL);
    }
    settings.beginGroup(name);
    settings.setValue("area",area);
    for (short i=0;i<4;i++){
        QString but=QString("button%1/").arg(i+1);
        settings.setValue(but+"description",buttons[i].description);
        settings.setValue(but+"keyDescription",buttons[i].keyDescription);
        settings.setValue(but+"keyAction",buttons[i].keyAction);
    }
}


